# Snake with Python and Pygame  

The goal of this project was to reproduce the *Snake* game in Python by using Pygame  
It includes a main menu, images, different difficulties and save files  

### Dependencies  

- [*Python 3.8*](https://www.python.org)  
- [*Pygame*](https://www.pygame.org)       `pip install pygame` 
- [*appdirs*](https://pypi.org/project/appdirs/)       `pip install appdirs`  

### How to launch the game  

1. Download the project
2. Launch snake.py         `python snake.py`  

### How to play  

Select your difficulty by clicking on its name or with the arrows. It changes the snake's speed  
Press **Return** or **Space** to play  
Move the snake with the **arrows**  
Try to eat as much apples as possible without touching the walls nor yourself  
Press `Escape` to pause the game
Your best scores will automatically be saved  

### What I used
- Python 3
- Pygame
- All sounds come from [freesound.org](https://www.freesound.org)  
- The font can be found [here](https://www.dafont.com/pixeled.font)
