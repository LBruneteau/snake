#!/usr/bin/python3.8
# pylint: disable=unbalanced-tuple-unpacking, bare-except, global-statement, invalid-name
'''
Snake game
Create a snake instance and use mainloop method to play
'''

import random
import sys
import os
import pickle
import pygame
import appdirs

def path(relative_path: str) -> str:
    """
    returns the absolute path
    works with pyinstaller
    """
    try:
        base_path = sys._MEIPASS #pylint: disable=no-member, protected-access
    except:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)


class Snake:

    '''
    Main part of the game
    Receive user inputs, move, eat apple etc...
    '''

    def __init__(self):
        '''
        Creation of the new snake, placed at 160, 160
        Images loaded
        '''
        self.snake: list = [(160, 160)]
        self.current_direction: tuple = (0, 32)
        self.next_direction: tuple = (0, 32)
        self.back: tuple = None #Back of the snake, is removed when moving if no apple

    def __len__(self):
        return len(self.snake)

    def move(self):
        '''
        Move the snake toward its next direction
        Go forward if try to go backward
        '''
        directions: set = {self.current_direction, self.next_direction}
        going_back: bool = directions in ({(0, 32), (0, -32)}, {(32, 0), (-32, 0)})
        self.current_direction = self.next_direction if not going_back else self.current_direction
        head = self.snake[0]
        changes: tuple = (head[0] + self.current_direction[0], head[1] + self.current_direction[1])
        self.snake.insert(0, changes)
        self.back = self.snake.pop()

    def grow_up(self):
        '''
        the snake grows up
        '''
        self.snake.append(self.back)

    def is_hit(self) -> bool:
        '''
        Determines if the snake is oob or hits itself
        '''
        if self.snake[0] in self.snake[1:]:
            return True
        elif self.snake[0][0] not in range(481):
            return True
        elif self.snake[0][1] not in range(481):
            return True
        return False

    def place_apple(self) ->tuple:
        '''
        place an apple in a random empty location
        '''
        possible_spawns: list = [(x, y) for x in range(0, 481, 32) for y in range(0, 481, 32)]
        for part in self.snake:
            possible_spawns.remove(part)
        return random.choice(possible_spawns)

    def print(self, window, head_img, body_img):
        '''
        Shows the snake on the scren
        '''
        for part in self.snake[1:]:
            window.blit(body_img, part)
        if self.current_direction in ((0, 32), (0, -32)):
            window.blit(head_img, self.snake[0])
        else:
            window.blit(pygame.transform.rotate(head_img, 90), self.snake[0])

    def use_input(self, key_pressed):
        '''
        Adapts the snake's next direction depending on the input
        key_pressed is a key attribute from a pygame keydown event
        if going backthe input isn't registered at all
        '''
        #Makes the link between the keys and vectors
        #Also used to see if a key is valid
        dict_dir: dict = {
            pygame.K_UP:(0, -32),
            pygame.K_DOWN:(0, 32),
            pygame.K_LEFT:(-32, 0),
            pygame.K_RIGHT:(32, 0)
        }
        if key_pressed in dict_dir.keys():
            direct: tuple = dict_dir[key_pressed]
            directions: set = {direct, self.current_direction}
            going_back: bool = directions in ({(0, 32), (0, -32)}, {(32, 0), (-32, 0)})
            if not going_back:
                self.next_direction = direct


    def eating_apple(self, apple: tuple) -> bool:
        '''
        returns true if the snake head is on the apple
        '''
        return self.snake[0] == apple

def read_file() -> dict:
    '''
    Reads a "best" file and return a dict of the best scores
    return a default if fail
    '''
    try:
        with open(appdirs.user_data_dir('snake', 'LBruneteau'), 'rb') as save:
            p = pickle.Unpickler(save)
            best_scores: dict = p.load()
    except Exception: # pylint: disable=broad-except
        best_scores: dict = {'EASY':0, 'NORMAL':0, 'HARD':0}
    return best_scores

def save_file():
    '''
    Saves the best scores in a binary file named "best"
    '''
    with open(appdirs.user_data_dir('snake', 'LBruneteau'), 'wb') as save:
        p = pickle.Pickler(save)
        p.dump(best)

def pause(window, snake, apple, score, diff) -> bool:
    '''
    Pauses the game
    Return False if the user clicked menu else True
    '''
    win = pygame.Surface((512, 512))

    WHITE: tuple = (255, 255, 255)
    BLACK: tuple = (0, 0, 0)    

    font = pygame.font.Font(path('Pixeled.ttf'), 15)
    continue_render = font.render("CONTINUE", True, WHITE, BLACK)
    quit_render = font.render("QUIT", True, WHITE)
    choice = True
    continue_rect = continue_render.get_rect()
    continue_rect.center = (170, 256)
    quit_rect = quit_render.get_rect()
    quit_rect.center = (340, 256)


    big_font = pygame.font.Font(path('Pixeled.ttf'), 35)
    pause_render = big_font.render("PAUSE", True, WHITE)
    pause_rect = pause_render.get_rect()
    pause_rect.midtop = (256, 64)

    background = pygame.image.load(path('background.png')).convert()
    apple_img = pygame.image.load(path('apple.png')).convert_alpha()
    head_img = pygame.image.load(path('snake_head.png')).convert_alpha()
    body_img = pygame.image.load(path('snake_body.png')).convert_alpha()

    font = pygame.font.Font(path('Pixeled.ttf'), 15)
    score_render = font.render(f'SCORE : {score}', True, WHITE)
    best_render = font.render(f'BEST : {best[diff]}', True, WHITE)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    return True
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    choice = not choice
                    if choice:
                        continue_render = font.render("CONTINUE", True, WHITE, BLACK)
                        quit_render = font.render("QUIT", True, WHITE)
                    else:
                        continue_render = font.render("CONTINUE", True, WHITE)
                        quit_render = font.render("QUIT", True, WHITE, BLACK)
                elif event.key == pygame.K_SPACE or event.key == pygame.K_RETURN:
                    return choice
            elif event.type == pygame.VIDEORESIZE:
                window = pygame.display.set_mode(event.size, pygame.RESIZABLE)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                if continue_rect.collidepoint(pos):
                    
                    return True
                elif quit_rect.collidepoint(pos):
                    
                    return False

        win.blit(background, (0, 0))
        snake.print(win, head_img, body_img)
        win.blit(apple_img, apple)
        win.blit(score_render, (0, -12))
        win.blit(best_render, (0, 20))
        win.blit(pause_render, pause_rect)
        win.blit(continue_render, continue_rect)
        win.blit(quit_render, quit_rect)
        window.blit(pygame.transform.scale(win, window.get_size()), (0, 0))
        pygame.display.flip()    

def game_over(window, win, won: bool, score: int):
    '''
    Displays a game over message
    if won = True it's a victory message
    the score is displayed
    '''

    msg: str = 'VICTORY' if won else 'GAME OVER'
    sound_name: str = 'game_over.wav'
    big_font = pygame.font.Font(path('Pixeled.ttf'), 35)
    render = big_font.render(msg, True, (255, 255, 255))
    rect = render.get_rect()
    rect.center = (256, 256)

    font = pygame.font.Font(path('Pixeled.ttf'), 15)
    score_render = font.render(f'SCORE : {score}', True, (255, 255, 255))
    score_rect = score_render.get_rect()
    score_rect.center = (256, 360)

    win.blit(render, rect)
    win.blit(score_render, score_rect)
    window.blit(pygame.transform.scale(win, window.get_size()), (0, 0))
    pygame.display.flip()

    #play the sound
    pygame.mixer.Sound(path(sound_name)).play()

    #2 second on game over
    STOPGAMEOVER = pygame.USEREVENT + 2
    pygame.time.set_timer(STOPGAMEOVER, 2000)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == STOPGAMEOVER:
                pygame.time.set_timer(STOPGAMEOVER, 0)
                return None
            elif event.type == pygame.KEYDOWN and event.key in (pygame.K_RETURN, pygame.K_SPACE):
                pygame.time.set_timer(STOPGAMEOVER, 0)
                return None
            elif event.type == pygame.VIDEORESIZE:
                window = pygame.display.set_mode(event.size, pygame.RESIZABLE)
                window.blit(pygame.transform.scale(win, window.get_size()), (0, 0))
                pygame.display.flip()

def game(window, difficulty: str):
    '''
    Allow you to play a game of snake
    ends up when dying or quitting
    best score is the best of the category
    difficulty is "EASY", "NORMAL" or "HARD"
    returns the score of the player and the best of rthe category
    '''
    global best
    win = pygame.Surface((512, 512))
    #Link between difficulty and speed
    #Also creating the move timer
    dict_diff: dict = {'EASY':200, 'NORMAL':115, 'HARD':90}
    MOVESNAKE = pygame.USEREVENT + 1
    timer: int = dict_diff[difficulty] if difficulty in dict_diff else 200
    pygame.time.set_timer(MOVESNAKE, timer)

    #Loading the needed images
    head_img = pygame.image.load(path('snake_head.png')).convert_alpha()
    body_img = pygame.image.load(path('snake_body.png')).convert_alpha()
    apple_img = pygame.image.load(path('apple.png')).convert_alpha()
    background = pygame.image.load(path('background.png')).convert_alpha()

    #Font and default text
    WHITE: tuple = (255, 255, 255)
    font = pygame.font.Font(path('Pixeled.ttf'), 15)
    score_render = font.render('SCORE : 0', True, WHITE)
    best_render = font.render(f'BEST : {best[difficulty]}', True, WHITE)

    #Where to put them
    SCORE_RECT: tuple = (0, -12)
    BEST_RECT: tuple = (0, 20)

    crunch = pygame.mixer.Sound(path('crunch.wav'))
    #Initialisation
    snake = Snake()
    apple: tuple = snake.place_apple()
    score: int = 0
    running: bool = True

    #Main loop
    while running:
        for event in pygame.event.get():
            #Using inputs
            if event.type == pygame.KEYDOWN:
                #Going back to the menu if escape pressed
                if event.key == pygame.K_ESCAPE:
                    if not pause(window, snake, apple, score, difficulty):
                        running = False
                else:
                    snake.use_input(event.key)
            #Only moving regularly but always register inputs
            elif event.type == MOVESNAKE:

                if snake.eating_apple(apple):
                    crunch.play()
                    apple = snake.place_apple()
                    snake.grow_up()
                    score += 1
                    best[difficulty] = max(score, best[difficulty])
                    score_render = font.render(f'SCORE : {score}', True, WHITE)
                    best_render = font.render(f'BEST : {best[difficulty]}', True, WHITE)
                if len(snake) == 256:
                    game_over(window, win, True, 255)
                    running = False
                else:
                    snake.move()
                    if snake.is_hit():
                        game_over(window, win, False, score)
                        running = False

            elif event.type == pygame.VIDEORESIZE:
                window = pygame.display.set_mode(event.size, pygame.RESIZABLE)

            elif event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        win.blit(background, (0, 0))
        win.blit(apple_img, apple)
        snake.print(win, head_img, body_img)
        win.blit(score_render, SCORE_RECT)
        win.blit(best_render, BEST_RECT)
        window.blit(pygame.transform.scale(win, window.get_size()), (0, 0))
        pygame.display.flip()


    pygame.time.set_timer(MOVESNAKE, 0)

def update_diff_render(font, difficulty: str) -> tuple:
    '''
    Regenerates the easy normal and hard buttons of the menu
    The chosen difficulty has a black background
    '''
    renders: list = [] #Will contain the 3 renders
    for diff in ('EASY', 'NORMAL', 'HARD'):
        if diff == difficulty:
            r = font.render(difficulty, True, (255, 255, 255), (0, 0, 0))
        else:
            r = font.render(diff, True, (255, 255, 255))

        renders.append(r)
    return tuple(renders)

def update_best_render(font, difficulty: str):
    '''
    Updates the highscore of the category
    shows a new render
    '''
    return font.render(f'BEST : {best[difficulty]}', True, (255, 255, 255))

def main_menu():
    '''
    To launch with the game
    generates a window, Puts a title
    prompt to choose a difficuklty
    launch a game
    uses save files
    '''

    #Creating the window
    window = pygame.display.set_mode((512, 512), pygame.RESIZABLE)
    win = window.copy()
    pygame.display.set_caption('Snake')
    pygame.display.set_icon(pygame.image.load(path('apple.png')))
    background = pygame.image.load(path('background.png')).convert()
    head = pygame.transform.rotate(pygame.image.load(path('snake_head.png')).convert_alpha(), 90)
    body = pygame.image.load(path('snake_body.png')).convert_alpha()
    apple = pygame.image.load(path('apple.png')).convert_alpha()

    WHITE: tuple = (255, 255, 255)

    diff: str = 'NORMAL'

    title_font = pygame.font.Font(path('Pixeled.ttf'), 30)
    title = title_font.render('SNAKE', True, WHITE)
    title_rect = title.get_rect()
    title_rect.midtop = (256, 64)

    font = pygame.font.Font(path('Pixeled.ttf'), 15)
    easy_render, normal_render, hard_render = update_diff_render(font, 'NORMAL')

    easy_place = easy_render.get_rect()
    normal_place = normal_render.get_rect()
    hard_place = hard_render.get_rect()
    
    easy_place.midtop = (128, 256)
    normal_place.midtop = (256, 256)
    hard_place.midtop = (384, 256)

    best_render = update_best_render(font, diff)
    tuto_render = font.render('PRESS RETURN OR SPACE TO PLAY', True, WHITE)

    best_rect = best_render.get_rect()
    best_rect.center = (256, 360)
    tuto_rect = tuto_render.get_rect()
    tuto_rect.center = (256, 480)

    running: bool = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

            elif event.type == pygame.MOUSEBUTTONDOWN:
                #Set the difficutly to the one chosen
                coord: tuple = pygame.mouse.get_pos()
                if easy_place.collidepoint(coord):
                    if diff != 'EASY':
                        diff = 'EASY'
                    else:
                        game(window, diff)
                elif normal_place.collidepoint(coord):
                    if diff != 'NORMAL':
                        diff = 'NORMAL'
                    else:
                        game(window, diff)
                elif hard_place.collidepoint(coord):
                    if diff != 'HARD':
                        diff = 'HARD'
                    else:
                        game(window, diff)
                easy_render, normal_render, hard_render = update_diff_render(font, diff)
                best_render = update_best_render(font, diff)
                
            
            #Launching the game if any key pressed
            elif event.type == pygame.KEYDOWN:
                if event.key in (pygame.K_RETURN, pygame.K_SPACE):
                    game(window, diff)
                    best_render = update_best_render(font, diff)
                elif event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key in (pygame.K_LEFT, pygame.K_RIGHT):
                    nb: int = -1 if event.key == pygame.K_LEFT else 1
                    list_diff: tuple = ('EASY', 'NORMAL', 'HARD')
                    diff = list_diff[(list_diff.index(diff) + nb) % 3]
                    easy_render, normal_render, hard_render = update_diff_render(font, diff)
                    best_render = update_best_render(font, diff)

            elif event.type == pygame.VIDEORESIZE:
                window = pygame.display.set_mode(event.dict['size'], pygame.RESIZABLE)


        #Drawing the window
        win.blit(background, (0, 0))
        win.blit(title, title_rect)
        win.blit(easy_render, easy_place)
        win.blit(normal_render, normal_place)
        win.blit(hard_render, hard_place)
        win.blit(tuto_render, tuto_rect)
        win.blit(best_render, best_rect)

        #Drawing the snake
        win.blit(apple, (64, 32))
        win.blit(head, (128, 32))
        for y in (1, 5):
            for x in range(5, 13):
                win.blit(body, (x * 32, y * 32))
        for y in range(2, 5):
            win.blit(body, (384, y * 32))
        window.blit(pygame.transform.scale(win, window.get_size()), (0, 0))
        pygame.display.flip()




if __name__ == "__main__":
    pygame.init()
    best = read_file()
    pygame.register_quit(save_file)
    main_menu()
